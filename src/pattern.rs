use rand::Rng;

use crate::Graph;

mod matcher;
pub mod selector;

#[cfg(test)]
mod tests;

use matcher::{Filter, Match, MatchContext, NoFilter};
use selector::{EdgeKind, VertexKind};

pub trait Pattern<V, E>: Graph {
    fn find_match_with<G, F>(&self, graph: G, filter: F) -> Option<Match<Self, G>>
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
        F: Filter<V, E>;

    fn find_match<G>(&self, graph: G) -> Option<Match<Self, G>>
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
    {
        self.find_match_with(graph, NoFilter)
    }

    fn find_random_match<G>(&self, graph: G, rng: &mut impl Rng) -> Option<Match<Self, G>>
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
    {
        let random = graph.random(rng);
        #[cfg(test)]
        {
            let indices: Vec<_> = random.vertex_indices().collect();
            println!("{:?}", indices);
        }
        self.find_match(&random)
    }
}

impl<T, V, E> Pattern<V, E> for T
where
    T: Graph,
    <T as Graph>::VertexLabel: Into<VertexKind>,
    <T as Graph>::EdgeLabel: Into<EdgeKind>,
{
    fn find_match_with<G, F>(&self, graph: G, filter: F) -> Option<Match<Self, G>>
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
        F: Filter<V, E>,
    {
        let pattern = self.mapping(Into::into, Into::into);
        MatchContext::new(&pattern, &graph, filter).find_match()
    }
}
