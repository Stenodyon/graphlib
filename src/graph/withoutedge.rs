use crate::Graph;

#[derive(Debug)]
pub struct WithoutEdge<G: Graph> {
    edge: G::EdgeIndex,
    inner: G,
}

impl<G: Graph> WithoutEdge<G> {
    pub fn new(edge: G::EdgeIndex, inner: G) -> Self {
        Self { edge, inner }
    }
}

impl<G: Graph> Graph for WithoutEdge<G> {
    type VertexIndex = <G as Graph>::VertexIndex;
    type EdgeIndex = <G as Graph>::EdgeIndex;

    type VertexLabel = <G as Graph>::VertexLabel;
    type EdgeLabel = <G as Graph>::EdgeLabel;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count() - 1
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        if edge == self.edge {
            None
        } else {
            self.inner.source(edge)
        }
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        if edge == self.edge {
            None
        } else {
            self.inner.target(edge)
        }
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner
            .find_edge(from, to)
            .filter(|&edge| edge != self.edge)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner.vertex_indices()
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner
            .incoming_edges(vertex)
            .filter(|&edge| edge != self.edge)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner
            .outgoing_edges(vertex)
            .filter(|&edge| edge != self.edge)
    }
}
