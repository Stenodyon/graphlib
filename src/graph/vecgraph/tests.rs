
use super::*;

#[test]
fn undirected() {
    let mut graph = VecGraph::new_undirected();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());
    assert!(graph.are_connected(a, b));
    assert!(graph.are_connected(b, a));

    assert!(graph.connected(a).count() == 1);
    assert!(graph.connected(b).count() == 1);
}

#[test]
fn directed() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());
    assert!(graph.are_connected(a, b));
    assert!(!graph.are_connected(b, a));

    assert!(graph.connected(a).count() == 1);
    assert!(graph.connected(b).count() == 0);
}

#[test]
fn vertices_mut() {
    let mut graph = VecGraph::<_, _, ()>::new_undirected();
    let a = graph.insert("a".to_string());
    let b = graph.insert("b".to_string());
    let c = graph.insert("c".to_string());
    graph.vertices_mut().for_each(|v| v.push('!'));
    assert!(graph.get(a) == Some(&"a!".to_string()));
    assert!(graph.get(b) == Some(&"b!".to_string()));
    assert!(graph.get(c) == Some(&"c!".to_string()));
}

#[test]
fn remove() {
    let mut graph = VecGraph::new_undirected();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    graph.connect(a, b, ());
    graph.connect(b, c, ());
    graph.connect(a, c, ());
    assert!(graph.are_connected(a, b));
    assert!(graph.are_connected(b, c));
    assert!(graph.are_connected(a, c));
    graph.remove(b);
    assert!(!graph.are_connected(a, b));
    assert!(!graph.are_connected(b, c));
    assert!(graph.are_connected(a, c));
    assert_eq!(graph.get(b), None);
    let b_ = graph.insert("b");
    assert_eq!(graph.get(b), None);
    assert!(graph.get(b_) == Some(&"b"));
}

#[test]
fn to_self_undirected() {
    let mut graph = VecGraph::new_undirected();
    let a = graph.insert("a");
    graph.connect(a, a, ());
    assert!(graph.are_connected(a, a));
    assert!(graph.connected(a).count() == 1);
}

#[test]
fn to_self_directed() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    graph.connect(a, a, ());
    assert!(graph.are_connected(a, a));
    assert!(graph.connected(a).count() == 1);
}

#[test]
fn incoming_outgoing() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, a, ());
    graph.connect(a, b, ());

    assert_eq!(graph.outgoing_edges(a).count(), 2);
    assert_eq!(graph.outgoing_edges(b).count(), 0);
    assert_eq!(graph.incoming_edges(a).count(), 1);
    assert_eq!(graph.incoming_edges(b).count(), 1);

    for edge in graph.outgoing_edges(a) {
        assert_eq!(graph.source(edge), Some(a));
    }
}

#[test]
fn reachable_three() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    graph.connect(a, b, ());
    graph.connect(b, c, ());

    assert!((&graph).is_reachable(a, c));
}

#[test]
fn unreachable_three() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    graph.connect(a, b, ());

    assert!(!(&graph).is_reachable(a, c));
}

#[test]
fn reachable_loop() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    let d = graph.insert("d");
    let e = graph.insert("e");
    graph.connect(a, b, ());
    graph.connect(b, c, ());
    graph.connect(c, a, ());
    graph.connect(c, d, ());
    graph.connect(c, e, ());

    assert!((&graph).is_reachable(a, e));
}

#[test]
fn reachable_self() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    let a = graph.insert("a");
    assert!((&graph).is_reachable(a, a));
}

#[test]
fn set_source() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    let e = graph.connect(a, c, ());

    assert!(graph.are_connected(a, c));
    assert!(!graph.are_connected(b, c));

    assert_eq!(graph.set_source(e, b), None);

    assert!(!graph.are_connected(a, c));
    assert!(graph.are_connected(b, c));
}

#[test]
fn set_target() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    let e = graph.connect(a, b, ());

    assert!(graph.are_connected(a, b));
    assert!(!graph.are_connected(a, c));

    assert_eq!(graph.set_target(e, c), None);

    assert!(!graph.are_connected(a, b));
    assert!(graph.are_connected(a, c));
}
