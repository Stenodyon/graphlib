#[derive(Debug, Clone)]
pub struct GenerationalVec<T>(Vec<Entry<T>>);

impl<T> Default for GenerationalVec<T> {
    fn default() -> Self {
        Self(Vec::default())
    }
}

#[derive(Debug, Clone)]
struct Entry<T> {
    generation: usize,
    value: Option<T>,
}

impl<T> Entry<T> {
    pub fn map<U>(self, fmap: impl FnMut(T) -> U) -> Entry<U> {
        let Entry { generation, value } = self;
        let value = value.map(fmap);
        Entry { generation, value }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Index {
    generation: usize,
    index: usize,
}

impl Index {
    pub fn generation(&self) -> usize {
        self.generation
    }

    pub fn index(&self) -> usize {
        self.index
    }
}

#[cfg(feature = "godot")]
mod godot {
    use super::*;
    use ::godot::builtin::meta::{ConvertError, GodotConvert};
    use ::godot::prelude::*;

    impl GodotConvert for Index {
        type Via = Array<u64>;
    }

    impl ToGodot for Index {
        fn to_godot(&self) -> Self::Via {
            Array::from(&[self.generation as u64, self.index as u64])
        }
    }

    impl FromGodot for Index {
        fn try_from_godot(via: Self::Via) -> Result<Self, ConvertError> {
            let this =
                Self {
                    generation: via.try_get(0).ok_or_else(|| {
                        ConvertError::with_cause_value("array too short", via.len())
                    })? as usize,
                    index: via.try_get(1).ok_or_else(|| {
                        ConvertError::with_cause_value("array too short", via.len())
                    })? as usize,
                };

            Ok(this)
        }
    }

    impl From<Array<u64>> for Index {
        fn from(value: Array<u64>) -> Self {
            Self {
                generation: value.get(0) as usize,
                index: value.get(1) as usize,
            }
        }
    }
}

impl<T> GenerationalVec<T> {
    #[inline(always)]
    pub fn new() -> Self {
        Self(Vec::new())
    }

    #[inline(always)]
    pub fn count(&self) -> usize {
        self.0.iter().filter(|entry| entry.value.is_some()).count()
    }

    pub fn get(&self, index: Index) -> Option<&T> {
        let entry = &self.0[index.index];
        if entry.generation != index.generation {
            return None;
        }
        entry.value.as_ref()
    }

    pub fn get_mut(&mut self, index: Index) -> Option<&mut T> {
        let entry = &mut self.0[index.index];
        if entry.generation != index.generation {
            return None;
        }
        entry.value.as_mut()
    }

    pub fn insert(&mut self, value: T) -> Index {
        if let Some((index, entry)) = self
            .0
            .iter_mut()
            .enumerate()
            .find(|(_, entry)| entry.value.is_none())
        {
            entry.generation += 1;
            entry.value = Some(value);
            Index {
                generation: entry.generation,
                index,
            }
        } else {
            let index = self.0.len();
            let generation = 1;
            self.0.push(Entry {
                generation,
                value: Some(value),
            });
            Index { generation, index }
        }
    }

    pub fn remove(&mut self, index: Index) -> Option<T> {
        let entry = &mut self.0[index.index];
        if entry.generation != index.generation {
            return None;
        }
        entry.value.take()
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.0.iter().filter_map(|entry| entry.value.as_ref())
    }

    pub fn iter_indices(&self) -> impl Iterator<Item = Index> + '_ {
        self.0.iter().enumerate().filter_map(|(index, entry)| {
            entry.value.as_ref().map(|_| Index {
                generation: entry.generation,
                index,
            })
        })
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.0.iter_mut().filter_map(|entry| entry.value.as_mut())
    }

    pub fn map<U>(self, mut fmap: impl FnMut(T) -> U) -> GenerationalVec<U> {
        let container = self
            .0
            .into_iter()
            .map(|entry| entry.map(&mut fmap))
            .collect();
        GenerationalVec(container)
    }
}

impl<T> std::ops::Index<Index> for GenerationalVec<T> {
    type Output = T;

    fn index(&self, index: Index) -> &Self::Output {
        self.get(index).unwrap()
    }
}

impl<T> std::ops::IndexMut<Index> for GenerationalVec<T> {
    fn index_mut(&mut self, index: Index) -> &mut Self::Output {
        self.get_mut(index).unwrap()
    }
}
