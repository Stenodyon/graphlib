use super::*;
use ::godot::builtin::meta::{ConvertError, GodotConvert};
use ::godot::prelude::*;

macro_rules! make_impls {
        ($x:ident, $($y:ident),+) => {
            make_impls! { $x }
            make_impls! { $($y),+ }
        };

        ($t:ident) => {
            impl GodotConvert for $t {
                type Via = Array<u64>;
            }

            impl ToGodot for $t {
                fn to_godot(&self) -> Self::Via {
                    self.0.to_godot()
                }
            }

            impl FromGodot for $t {
                fn try_from_godot(via: Self::Via) -> Result<Self, ConvertError> {
                    Index::try_from_godot(via).map(Self)
                }
            }

            impl From<Array<u64>> for $t {
                fn from(value: Array<u64>) -> Self {
                    Self(Index::from(value))
                }
            }
        };
    }

make_impls! { VertexIndex, EdgeIndex }
