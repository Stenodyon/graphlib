use crate::VertexIndex;

pub trait Directedness {
    fn embed_edge(edge: (VertexIndex, VertexIndex)) -> (VertexIndex, VertexIndex);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Undirected;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Directed;

impl Directedness for Undirected {
    #[inline(always)]
    fn embed_edge((a, b): (VertexIndex, VertexIndex)) -> (VertexIndex, VertexIndex) {
        let [a, b] = {
            let mut tmp = [a, b];
            tmp.sort();
            tmp
        };

        (a, b)
    }
}

impl Directedness for Directed {
    #[inline(always)]
    fn embed_edge(edge: (VertexIndex, VertexIndex)) -> (VertexIndex, VertexIndex) {
        edge
    }
}
