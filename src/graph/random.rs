use std::collections::HashMap;

use rand::seq::SliceRandom;
use rand::Rng;

use crate::Graph;

#[derive(Debug)]
pub struct Random<G: Graph> {
    shuffled: HashMap<G::VertexIndex, G::VertexIndex>,
    inner: G,
}

impl<G: Graph> Random<G> {
    pub fn new(inner: G, rng: &mut impl Rng) -> Self {
        let mut vertices: Vec<_> = inner.vertex_indices().collect();
        vertices.shuffle(rng);

        let shuffled = inner
            .vertex_indices()
            .zip(vertices.iter().copied())
            .collect();

        Self { inner, shuffled }
    }
}

impl<G: Graph> Graph for Random<G> {
    type VertexIndex = <G as Graph>::VertexIndex;
    type EdgeIndex = <G as Graph>::EdgeIndex;

    type VertexLabel = <G as Graph>::VertexLabel;
    type EdgeLabel = <G as Graph>::EdgeLabel;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner.find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner
            .vertex_indices()
            .map(|vertex| self.shuffled[&vertex])
    }
}
