
use super::*;

#[test]
fn edge_count() {
    let grid = &Grid::new(20, 10, ());

    assert_eq!(
        grid.vertex_indices()
            .map(|index| grid.outgoing_edges(index).count())
            .min(),
        Some(2)
    );
    assert_eq!(
        grid.vertex_indices()
            .map(|index| grid.outgoing_edges(index).count())
            .max(),
        Some(4)
    );

    assert_eq!(
        grid.vertex_indices()
            .map(|index| grid.incoming_edges(index).count())
            .min(),
        Some(2)
    );
    assert_eq!(
        grid.vertex_indices()
            .map(|index| grid.incoming_edges(index).count())
            .max(),
        Some(4)
    );
}
