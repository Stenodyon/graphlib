use std::marker::PhantomData;

use crate::Graph;

#[derive(Debug)]
pub struct CopiedVertex<'a, G, T> {
    inner: G,
    _phantom: PhantomData<&'a T>,
}

impl<'a, G, T> CopiedVertex<'a, G, T>
where
    G: Graph<VertexLabel = &'a T>,
    T: Copy,
{
    pub fn new(inner: G) -> Self {
        Self {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<'a, G, T> Graph for CopiedVertex<'a, G, T>
where
    G: Graph<VertexLabel = &'a T>,
    T: Copy,
{
    type VertexIndex = G::VertexIndex;
    type EdgeIndex = G::EdgeIndex;

    type VertexLabel = T;
    type EdgeLabel = G::EdgeLabel;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex).copied()
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner.find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner.vertex_indices()
    }
}

#[derive(Debug)]
pub struct CopiedEdge<'a, G, T> {
    inner: G,
    _phantom: PhantomData<&'a T>,
}

impl<'a, G, T> CopiedEdge<'a, G, T>
where
    G: Graph<EdgeLabel = &'a T>,
    T: Copy,
{
    pub fn new(inner: G) -> Self {
        Self {
            inner,
            _phantom: PhantomData,
        }
    }
}

impl<'a, G, T> Graph for CopiedEdge<'a, G, T>
where
    G: Graph<EdgeLabel = &'a T>,
    T: Copy,
{
    type VertexIndex = G::VertexIndex;
    type EdgeIndex = G::EdgeIndex;

    type VertexLabel = G::VertexLabel;
    type EdgeLabel = T;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge).copied()
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner.find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner.vertex_indices()
    }
}
