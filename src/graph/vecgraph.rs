use std::{collections::HashSet, marker::PhantomData};

mod directedness;
mod generational;

#[cfg(feature = "godot")]
mod godot;

#[cfg(test)]
mod tests;

pub use directedness::*;
use generational::{GenerationalVec, Index};

use crate::Graph;

#[derive(Debug, Clone)]
pub struct VecGraph<D: Directedness, V, E> {
    vertices: GenerationalVec<V>,
    edges: GenerationalVec<Edge<E>>,
    phantom: PhantomData<D>,
}

pub type UndirectedGraph<V, E> = VecGraph<Undirected, V, E>;
pub type DirectedGraph<V, E> = VecGraph<Directed, V, E>;

impl<D: Directedness, V, E> Default for VecGraph<D, V, E> {
    fn default() -> Self {
        Self {
            vertices: GenerationalVec::new(),
            edges: GenerationalVec::new(),
            phantom: PhantomData,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct VertexIndex(Index);

impl std::fmt::Debug for VertexIndex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("VertexIndex")
            .field(&self.0.generation())
            .field(&self.0.index())
            .finish()
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct EdgeIndex(Index);

impl std::fmt::Debug for EdgeIndex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("EdgeIndex")
            .field(&self.0.generation())
            .field(&self.0.index())
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Edge<E> {
    vertices: (VertexIndex, VertexIndex),
    value: E,
}

impl<E> Edge<E> {
    pub fn map<U>(self, mut fmap: impl FnMut(E) -> U) -> Edge<U> {
        let Self { vertices, value } = self;

        Edge {
            vertices,
            value: fmap(value),
        }
    }
}

impl<V, E> VecGraph<Undirected, V, E> {
    #[inline]
    pub fn new_undirected() -> Self {
        Self::new()
    }
}

impl<V, E> VecGraph<Directed, V, E> {
    #[inline]
    pub fn new_directed() -> Self {
        Self::new()
    }
}

impl<D: Directedness, V, E> VecGraph<D, V, E> {
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline(always)]
    pub fn vertex_count(&self) -> usize {
        self.vertices.count()
    }

    #[inline(always)]
    pub fn edge_count(&self) -> usize {
        self.edges.count()
    }

    #[inline(always)]
    pub fn get(&self, VertexIndex(index): VertexIndex) -> Option<&V> {
        self.vertices.get(index)
    }

    #[inline(always)]
    pub fn get_mut(&mut self, VertexIndex(index): VertexIndex) -> Option<&mut V> {
        self.vertices.get_mut(index)
    }

    #[inline(always)]
    pub fn get_edge(&self, edge: EdgeIndex) -> Option<&E> {
        self.get_entry(edge).map(|edge| &edge.value)
    }

    #[inline(always)]
    pub fn get_edge_mut(&mut self, edge: EdgeIndex) -> Option<&mut E> {
        self.get_entry_mut(edge).map(|edge| &mut edge.value)
    }

    #[inline(always)]
    fn get_entry(&self, EdgeIndex(index): EdgeIndex) -> Option<&Edge<E>> {
        self.edges.get(index)
    }

    #[inline(always)]
    fn get_entry_mut(&mut self, EdgeIndex(index): EdgeIndex) -> Option<&mut Edge<E>> {
        self.edges.get_mut(index)
    }

    pub fn get_many_mut(&mut self, indices: &[VertexIndex]) -> Vec<&mut V> {
        let mut seen = HashSet::new();
        indices.iter().for_each(|index| {
            if seen.contains(index) {
                panic!("get_many_edges_mut: same index");
            }
            seen.insert(*index);
        });

        indices
            .iter()
            .map(|&index| self.get_mut(index).unwrap() as *mut _)
            .map(|ptr| unsafe { &mut *ptr })
            .collect()
    }

    pub fn get_many_edges_mut(&mut self, indices: &[EdgeIndex]) -> Vec<&mut E> {
        let mut seen = HashSet::new();
        indices.iter().for_each(|index| {
            if seen.contains(index) {
                panic!("get_many_edges_mut: same index");
            }
            seen.insert(*index);
        });

        indices
            .iter()
            .map(|&index| self.get_edge_mut(index).unwrap() as *mut _)
            .map(|ptr| unsafe { &mut *ptr })
            .collect()
    }

    #[inline(always)]
    pub fn vertices(&self) -> impl Iterator<Item = &V> {
        self.vertices.iter()
    }

    #[inline(always)]
    pub fn vertices_mut(&mut self) -> impl Iterator<Item = &mut V> {
        self.vertices.iter_mut()
    }

    #[inline(always)]
    pub fn vertex_indices(&self) -> impl Iterator<Item = VertexIndex> + '_ {
        self.vertices.iter_indices().map(VertexIndex)
    }

    #[inline(always)]
    pub fn edge_indices(&self) -> impl Iterator<Item = EdgeIndex> + '_ {
        self.edges.iter_indices().map(EdgeIndex)
    }

    #[inline(always)]
    pub fn insert(&mut self, value: V) -> VertexIndex {
        VertexIndex(self.vertices.insert(value))
    }

    pub fn connect(&mut self, a: VertexIndex, b: VertexIndex, value: E) -> EdgeIndex {
        let (a, b) = D::embed_edge((a, b));

        if let Some(index) = self
            .edges
            .iter_indices()
            .find(|&index| self.edges.get(index).unwrap().vertices == (a, b))
        {
            return EdgeIndex(index);
        }

        let index = self.edges.insert(Edge {
            vertices: (a, b),
            value,
        });

        EdgeIndex(index)
    }

    pub fn disconnect(&mut self, a: VertexIndex, b: VertexIndex) -> Option<E> {
        let (a, b) = D::embed_edge((a, b));

        let Some(index) = self
            .edges
            .iter_indices()
            .find(|&index| self.edges.get(index).unwrap().vertices == (a, b))
        else {
            return None;
        };

        self.edges.remove(index).map(|edge| edge.value)
    }

    pub fn remove(&mut self, vertex: VertexIndex) -> Option<V> {
        let value = self.vertices.remove(vertex.0)?;

        let connected: Vec<_> = self.connected(vertex).collect();

        for connection in connected {
            self.disconnect(vertex, connection);
        }

        Some(value)
    }

    pub fn remove_edge(&mut self, EdgeIndex(edge): EdgeIndex) -> Option<E> {
        let Edge {
            vertices: (a, b), ..
        } = *self.edges.get(edge)?;

        self.disconnect(a, b)
    }

    pub fn are_connected(&self, a: VertexIndex, b: VertexIndex) -> bool {
        let (a, b) = D::embed_edge((a, b));

        self.edges.iter().any(|edge| edge.vertices == (a, b))
    }

    pub fn find_edge(&self, a: VertexIndex, b: VertexIndex) -> Option<EdgeIndex> {
        let (a, b) = D::embed_edge((a, b));

        self.edges
            .iter_indices()
            .find(|&edge| self.edges[edge].vertices == (a, b))
            .map(EdgeIndex)
    }

    pub fn connected(&self, vertex: VertexIndex) -> impl Iterator<Item = VertexIndex> + '_ {
        self.edges.iter_indices().filter_map(move |edge_index| {
            let (from, to) = self.edges.get(edge_index).unwrap().vertices;
            if (from, to) == (vertex, to) {
                Some(to)
            } else if (from, to) == D::embed_edge((vertex, from)) {
                Some(from)
            } else {
                None
            }
        })
    }

    pub fn outgoing_edges(&self, vertex: VertexIndex) -> impl Iterator<Item = EdgeIndex> + '_ {
        self.edges.iter_indices().filter_map(move |edge_index| {
            let (from, to) = self.edges.get(edge_index).unwrap().vertices;
            if (from, to) == (vertex, to) || (from, to) == D::embed_edge((vertex, from)) {
                Some(EdgeIndex(edge_index))
            } else {
                None
            }
        })
    }

    pub fn incoming_edges(&self, vertex: VertexIndex) -> impl Iterator<Item = EdgeIndex> + '_ {
        self.edges.iter_indices().filter_map(move |edge_index| {
            let (from, to) = self.edges.get(edge_index).unwrap().vertices;
            if (from, to) == (from, vertex) || (from, to) == D::embed_edge((to, vertex)) {
                Some(EdgeIndex(edge_index))
            } else {
                None
            }
        })
    }

    pub fn map<U>(self, fmap: impl FnMut(V) -> U) -> VecGraph<D, U, E> {
        let VecGraph {
            vertices, edges, ..
        } = self;

        let vertices = vertices.map(fmap);

        VecGraph {
            vertices,
            edges,
            phantom: PhantomData,
        }
    }

    pub fn map_edges<U>(self, mut fmap: impl FnMut(E) -> U) -> VecGraph<D, V, U> {
        let VecGraph {
            vertices, edges, ..
        } = self;

        let edges = edges.map(|edge| edge.map(&mut fmap));

        VecGraph {
            vertices,
            edges,
            phantom: PhantomData,
        }
    }

    pub fn bimap<U, W>(
        self,
        vmap: impl FnMut(V) -> U,
        mut emap: impl FnMut(E) -> W,
    ) -> VecGraph<D, U, W> {
        let Self {
            vertices, edges, ..
        } = self;

        let vertices = vertices.map(vmap);
        let edges = edges.map(|edge| edge.map(&mut emap));

        VecGraph {
            vertices,
            edges,
            phantom: PhantomData,
        }
    }
}

impl<V, E> VecGraph<Directed, V, E> {
    pub fn source(&self, EdgeIndex(edge): EdgeIndex) -> Option<VertexIndex> {
        self.edges.get(edge).map(|edge| edge.vertices.0)
    }

    pub fn target(&self, EdgeIndex(edge): EdgeIndex) -> Option<VertexIndex> {
        self.edges.get(edge).map(|edge| edge.vertices.1)
    }

    pub fn set_source(&mut self, edge: EdgeIndex, new_source: VertexIndex) -> Option<E> {
        let target = self.target(edge)?;
        let return_value = self
            .find_edge(new_source, target)
            .and_then(|old_edge| self.remove_edge(old_edge));
        self.get_entry_mut(edge)?.vertices.0 = new_source;
        return_value
    }

    pub fn set_target(&mut self, edge: EdgeIndex, new_target: VertexIndex) -> Option<E> {
        let source = self.source(edge)?;
        let return_value = self
            .find_edge(source, new_target)
            .and_then(|old_edge| self.remove_edge(old_edge));
        self.get_entry_mut(edge)?.vertices.1 = new_target;
        return_value
    }
}

impl<D: Directedness, V, E> std::ops::Index<VertexIndex> for VecGraph<D, V, E> {
    type Output = V;

    fn index(&self, index: VertexIndex) -> &Self::Output {
        self.get(index).unwrap()
    }
}

impl<D: Directedness, V, E> std::ops::IndexMut<VertexIndex> for VecGraph<D, V, E> {
    fn index_mut(&mut self, index: VertexIndex) -> &mut Self::Output {
        self.get_mut(index).unwrap()
    }
}

impl<D: Directedness, V, E> std::ops::Index<EdgeIndex> for VecGraph<D, V, E> {
    type Output = E;

    fn index(&self, index: EdgeIndex) -> &Self::Output {
        self.get_edge(index).unwrap()
    }
}

impl<D: Directedness, V, E> std::ops::IndexMut<EdgeIndex> for VecGraph<D, V, E> {
    fn index_mut(&mut self, index: EdgeIndex) -> &mut Self::Output {
        self.get_edge_mut(index).unwrap()
    }
}

impl<'a, V, E> Graph for &'a VecGraph<Directed, V, E> {
    type VertexIndex = VertexIndex;
    type EdgeIndex = EdgeIndex;

    type VertexLabel = &'a V;
    type EdgeLabel = &'a E;

    fn vertex_count(&self) -> usize {
        VecGraph::vertex_count(self)
    }

    fn edge_count(&self) -> usize {
        VecGraph::edge_count(self)
    }

    fn vertex_label(&self, vertex: VertexIndex) -> Option<Self::VertexLabel> {
        self.get(vertex)
    }

    fn edge_label(&self, edge: EdgeIndex) -> Option<Self::EdgeLabel> {
        self.get_edge(edge)
    }

    fn source(&self, edge: EdgeIndex) -> Option<VertexIndex> {
        VecGraph::source(self, edge)
    }

    fn target(&self, edge: EdgeIndex) -> Option<VertexIndex> {
        VecGraph::target(self, edge)
    }

    fn find_edge(&self, from: VertexIndex, to: VertexIndex) -> Option<EdgeIndex> {
        VecGraph::find_edge(self, from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        VecGraph::incoming_edges(self, vertex)
    }

    fn outgoing_edges(&self, vertex: VertexIndex) -> impl Iterator<Item = EdgeIndex> {
        VecGraph::outgoing_edges(self, vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = VertexIndex> {
        VecGraph::vertex_indices(self)
    }

    fn are_connected(&self, a: VertexIndex, b: VertexIndex) -> bool {
        VecGraph::are_connected(self, a, b)
    }
}

impl<D: Directedness, V, E> std::ops::Index<VertexIndex> for &VecGraph<D, V, E> {
    type Output = V;

    fn index(&self, index: VertexIndex) -> &Self::Output {
        self.get(index).unwrap()
    }
}

impl<D: Directedness, V, E> std::ops::Index<EdgeIndex> for &VecGraph<D, V, E> {
    type Output = E;

    fn index(&self, index: EdgeIndex) -> &Self::Output {
        self.get_edge(index).unwrap()
    }
}
