use crate::Graph;

#[cfg(test)]
mod tests;

pub struct Grid<T> {
    width: usize,
    height: usize,
    data: Vec<T>,
}

impl<T> Grid<T> {
    pub fn new(width: usize, height: usize, value: T) -> Self
    where
        T: Copy,
    {
        let data = vec![value; width * height];

        Self {
            width,
            height,
            data,
        }
    }

    pub fn new_with(width: usize, height: usize, mut fun: impl FnMut(usize, usize) -> T) -> Self {
        let data = (0..width)
            .flat_map(|col| (0..height).map(move |row| (row, col)))
            .map(|(row, col)| fun(row, col))
            .collect();

        Self {
            width,
            height,
            data,
        }
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

    pub fn to_coords(&self, index: usize) -> Option<(usize, usize)> {
        (index < self.width * self.height)
            .then(|| (index.rem_euclid(self.width), index.div_euclid(self.width)))
    }

    pub fn from_coords(&self, x: usize, y: usize) -> Option<usize> {
        (x < self.width && y < self.height).then_some(x * self.width + y)
    }

    pub fn step(&self, index: usize, dir: Dir) -> Option<usize> {
        let (x, y) = self.to_coords(index)?;
        match dir {
            Dir::Up => (y != 0).then(|| self.from_coords(x, y - 1)).flatten(),
            Dir::Down => (y < self.width - 1)
                .then(|| self.from_coords(x, y + 1))
                .flatten(),

            Dir::Left => (x != 0).then(|| self.from_coords(x - 1, y)).flatten(),
            Dir::Right => (x < self.height - 1)
                .then(|| self.from_coords(x + 1, y))
                .flatten(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    pub fn opposite(self) -> Self {
        match self {
            Dir::Up => Dir::Down,
            Dir::Down => Dir::Up,
            Dir::Left => Dir::Right,
            Dir::Right => Dir::Left,
        }
    }
}

const DIRS: [Dir; 4] = [Dir::Up, Dir::Down, Dir::Left, Dir::Right];

impl<'a, T> Graph for &'a Grid<T> {
    type VertexIndex = usize;
    type EdgeIndex = (usize, Dir);

    type VertexLabel = &'a T;
    type EdgeLabel = ();

    fn vertex_count(&self) -> usize {
        self.width * self.height
    }

    fn edge_count(&self) -> usize {
        (self.width.max(1) - 1) * self.height * 2 + (self.height.max(1) - 1) * self.width * 2
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.data.get(vertex)
    }

    fn edge_label(&self, (index, dir): Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.step(index, dir).map(|_| ())
    }

    fn source(&self, (index, dir): Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.step(index, dir).map(|_| index)
    }

    fn target(&self, (index, dir): Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.step(index, dir)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        DIRS.iter()
            .copied()
            .find(|dir| self.step(from, *dir) == Some(to))
            .map(|dir| (from, dir))
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        DIRS.iter().copied().filter_map(move |dir| {
            self.step(vertex, dir)
                .map(|vertex| (vertex, dir.opposite()))
        })
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        DIRS.iter()
            .copied()
            .map(move |dir| (vertex, dir))
            .filter(|&(vertex, dir)| self.step(vertex, dir).is_some())
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        0..self.width * self.height
    }
}
