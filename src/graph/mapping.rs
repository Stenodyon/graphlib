use std::marker::PhantomData;

use crate::Graph;

#[derive(Debug)]
pub struct MappingVertices<G, U, F> {
    inner: G,
    fmap: F,
    _phantom: PhantomData<U>,
}

impl<G, U, F> MappingVertices<G, U, F> {
    pub fn new(inner: G, fmap: F) -> Self {
        Self {
            inner,
            fmap,
            _phantom: PhantomData,
        }
    }
}

impl<G, U, F> Graph for MappingVertices<G, U, F>
where
    G: Graph,
    F: Fn(G::VertexLabel) -> U,
{
    type VertexIndex = G::VertexIndex;
    type EdgeIndex = G::EdgeIndex;

    type VertexLabel = U;
    type EdgeLabel = G::EdgeLabel;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex).map(&self.fmap)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner.find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner.vertex_indices()
    }
}

#[derive(Debug)]
pub struct MappingEdges<G, U, F> {
    inner: G,
    fmap: F,
    _phantom: PhantomData<U>,
}

impl<G, U, F> MappingEdges<G, U, F> {
    pub fn new(inner: G, fmap: F) -> Self {
        Self {
            inner,
            fmap,
            _phantom: PhantomData,
        }
    }
}

impl<G, U, F> Graph for MappingEdges<G, U, F>
where
    G: Graph,
    F: Fn(G::EdgeLabel) -> U,
{
    type VertexIndex = G::VertexIndex;
    type EdgeIndex = G::EdgeIndex;

    type VertexLabel = G::VertexLabel;
    type EdgeLabel = U;

    fn vertex_count(&self) -> usize {
        self.inner.vertex_count()
    }

    fn edge_count(&self) -> usize {
        self.inner.edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        self.inner.vertex_label(vertex)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        self.inner.edge_label(edge).map(&self.fmap)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        self.inner.target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        self.inner.find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        self.inner.outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        self.inner.vertex_indices()
    }
}
