use std::collections::{HashSet, VecDeque};
use std::hash::Hash;

use rand::Rng;

mod copied;
mod grid;
mod mapping;
mod random;
mod vecgraph;
mod withoutedge;

pub use copied::*;
pub use grid::*;
pub use mapping::*;
pub use random::*;
pub use vecgraph::*;
pub use withoutedge::*;

pub trait Graph: Sized {
    type VertexIndex: std::fmt::Debug + Copy + PartialEq + Eq + Hash;
    type EdgeIndex: std::fmt::Debug + Copy + PartialEq + Eq + Hash;

    type VertexLabel;
    type EdgeLabel;

    fn vertex_count(&self) -> usize;
    fn edge_count(&self) -> usize;

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel>;
    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel>;

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex>;
    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex>;

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex>;
    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex>;
    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex>;

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex>;

    fn are_connected(&self, a: Self::VertexIndex, b: Self::VertexIndex) -> bool {
        self.find_edge(a, b).is_some()
    }

    fn is_reachable(&self, a: Self::VertexIndex, b: Self::VertexIndex) -> bool {
        let mut visited = HashSet::with_capacity(self.vertex_count());
        let mut to_visit = VecDeque::with_capacity(self.edge_count());
        to_visit.push_back(a);

        while let Some(vertex) = to_visit.pop_front() {
            if vertex == b {
                return true;
            }
            visited.insert(vertex);

            for neighbor in self
                .outgoing_edges(vertex)
                .map(|edge| self.target(edge).unwrap())
            {
                if visited.contains(&neighbor) {
                    continue;
                }

                to_visit.push_back(neighbor);
            }
        }

        false
    }

    fn without_edge(self, edge: Self::EdgeIndex) -> WithoutEdge<Self> {
        WithoutEdge::new(edge, self)
    }

    fn random(self, rng: &mut impl Rng) -> Random<Self> {
        Random::new(self, rng)
    }

    fn mapping_vertices<F, U>(self, fmap: F) -> MappingVertices<Self, U, F>
    where
        F: Fn(Self::VertexLabel) -> U,
    {
        MappingVertices::new(self, fmap)
    }

    fn mapping_edges<F, U>(self, fmap: F) -> MappingEdges<Self, U, F>
    where
        F: Fn(Self::EdgeLabel) -> U,
    {
        MappingEdges::new(self, fmap)
    }

    fn mapping<FV, FE, V, E>(
        self,
        vmap: FV,
        emap: FE,
    ) -> MappingEdges<MappingVertices<Self, V, FV>, E, FE>
    where
        FV: Fn(Self::VertexLabel) -> V,
        FE: Fn(Self::EdgeLabel) -> E,
    {
        MappingEdges::new(MappingVertices::new(self, vmap), emap)
    }

    fn copied_vertices<'a, T>(self) -> CopiedVertex<'a, Self, T>
    where
        T: Copy,
        Self: Sized + Graph<VertexLabel = &'a T>,
    {
        CopiedVertex::new(self)
    }

    fn copied_edges<'a, T>(self) -> CopiedEdge<'a, Self, T>
    where
        T: Copy,
        Self: Sized + Graph<EdgeLabel = &'a T>,
    {
        CopiedEdge::new(self)
    }

    fn copied<'a, V, E>(self) -> CopiedEdge<'a, CopiedVertex<'a, Self, V>, E>
    where
        V: Copy,
        E: Copy,
        Self: Sized + Graph<VertexLabel = &'a V, EdgeLabel = &'a E>,
    {
        CopiedEdge::new(CopiedVertex::new(self))
    }
}

impl<'a, T: Graph> Graph for &'a T {
    type VertexIndex = <T as Graph>::VertexIndex;
    type EdgeIndex = <T as Graph>::EdgeIndex;

    type VertexLabel = <T as Graph>::VertexLabel;
    type EdgeLabel = <T as Graph>::EdgeLabel;

    fn vertex_count(&self) -> usize {
        (*self).vertex_count()
    }

    fn edge_count(&self) -> usize {
        (*self).edge_count()
    }

    fn vertex_label(&self, vertex: Self::VertexIndex) -> Option<Self::VertexLabel> {
        (*self).vertex_label(vertex)
    }

    fn edge_label(&self, edge: Self::EdgeIndex) -> Option<Self::EdgeLabel> {
        (*self).edge_label(edge)
    }

    fn source(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        (*self).source(edge)
    }

    fn target(&self, edge: Self::EdgeIndex) -> Option<Self::VertexIndex> {
        (*self).target(edge)
    }

    fn find_edge(&self, from: Self::VertexIndex, to: Self::VertexIndex) -> Option<Self::EdgeIndex> {
        (*self).find_edge(from, to)
    }

    fn incoming_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        (*self).incoming_edges(vertex)
    }

    fn outgoing_edges(&self, vertex: Self::VertexIndex) -> impl Iterator<Item = Self::EdgeIndex> {
        (*self).outgoing_edges(vertex)
    }

    fn vertex_indices(&self) -> impl Iterator<Item = Self::VertexIndex> {
        (*self).vertex_indices()
    }
}
