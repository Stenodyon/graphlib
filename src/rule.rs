use crate::{pattern::Pattern, Directed, Graph, VecGraph, VertexIndex};

#[cfg(test)]
mod tests;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum RuleVertex<VertexIndex> {
    NewNode(usize),
    KeptNode(VertexIndex),
    Deleted,
}

struct Merge<Index> {
    witness: Index,
    merges: Vec<Index>,
}

struct EdgeCreate<VertexIndex, E> {
    source: RuleVertex<VertexIndex>,
    target: RuleVertex<VertexIndex>,
    creator: Box<dyn Fn() -> E>,
}

pub trait RuleProcessor<V, E> {
    fn process_vertices(&mut self, _processed: &mut V, _merging: &[&V]) {}
    fn process_edges(&mut self, _processed: &mut E, _merging: &[&E]) {}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct NoProcessing;

impl<V, E> RuleProcessor<V, E> for NoProcessing {}

pub struct Rule<P, V, E>
where
    P: Graph,
{
    pattern: P,

    vertex_create: Vec<Box<dyn Fn() -> V>>,
    vertex_merge: Vec<Merge<P::VertexIndex>>,
    vertex_delete: Vec<P::VertexIndex>,
    edge_create: Vec<EdgeCreate<P::VertexIndex, E>>,
    edge_merge: Vec<Merge<P::EdgeIndex>>,
    edge_change_source: Vec<(P::EdgeIndex, RuleVertex<P::VertexIndex>)>,
    edge_change_target: Vec<(P::EdgeIndex, RuleVertex<P::VertexIndex>)>,
    edge_delete: Vec<P::EdgeIndex>,
}

impl<P, V, E> Rule<P, V, E>
where
    P: Graph,
{
    pub fn new(pattern: P) -> Self {
        Self {
            pattern,

            vertex_create: Vec::default(),
            vertex_merge: Vec::default(),
            vertex_delete: Vec::default(),
            edge_create: Vec::default(),
            edge_merge: Vec::default(),
            edge_change_source: Vec::default(),
            edge_change_target: Vec::default(),
            edge_delete: Vec::default(),
        }
    }

    pub fn try_apply(
        &self,
        graph: &mut VecGraph<Directed, V, E>,
        mut processor: impl RuleProcessor<V, E>,
    ) -> bool
    where
        for<'x> P: Pattern<&'x V, &'x E>,
    {
        let Some(pattern_match) = self.pattern.find_match(graph as &_) else {
            return false;
        };

        for edge in self
            .edge_delete
            .iter()
            .copied()
            .map(|edge| pattern_match.edge(edge).unwrap())
        {
            graph.remove_edge(edge);
        }

        let new_vertices: Vec<_> = self
            .vertex_create
            .iter()
            .map(|func| graph.insert(func()))
            .collect();

        let get_rule_vertex = |vertex: RuleVertex<P::VertexIndex>| -> VertexIndex {
            match vertex {
                RuleVertex::NewNode(n) => new_vertices[n],

                RuleVertex::KeptNode(vertex) => {
                    let vertex = match self
                        .vertex_merge
                        .iter()
                        .find(|merge| vertex == merge.witness || merge.merges.contains(&vertex))
                    {
                        Some(merge) => merge.witness,
                        None => vertex,
                    };
                    pattern_match.vertex(vertex).unwrap()
                }

                RuleVertex::Deleted => unreachable!(),
            }
        };

        self.edge_merge.iter().for_each(|merge| {
            let mut indices = merge.merges.clone();
            indices.push(merge.witness);
            let indices: Vec<_> = indices
                .into_iter()
                .map(|v| pattern_match.edge(v).unwrap())
                .collect();
            let mut iter = graph.get_many_edges_mut(&indices).into_iter().rev();
            let witness = iter.next().unwrap();
            let merges: Vec<_> = iter.map(|e| e as &_).collect();
            processor.process_edges(witness, &merges);
            merge.merges.iter().for_each(|&edge| {
                graph.remove_edge(pattern_match.edge(edge).unwrap());
            });
        });

        self.edge_change_source
            .iter()
            .for_each(|&(edge, new_source)| {
                let new_source = get_rule_vertex(new_source);
                graph.set_source(pattern_match.edge(edge).unwrap(), new_source);
            });

        self.edge_change_target
            .iter()
            .for_each(|&(edge, new_target)| {
                let new_target = get_rule_vertex(new_target);
                graph.set_target(pattern_match.edge(edge).unwrap(), new_target);
            });

        self.vertex_merge.iter().for_each(|merge| {
            let mut indices = merge.merges.clone();
            indices.push(merge.witness);
            let indices: Vec<_> = indices
                .into_iter()
                .map(|v| pattern_match.vertex(v).unwrap())
                .collect();
            let mut iter = graph.get_many_mut(&indices).into_iter().rev();
            let witness = iter.next().unwrap();
            let merges: Vec<_> = iter.map(|v| v as &_).collect();
            processor.process_vertices(witness, &merges);
            merge.merges.iter().for_each(|&vertex| {
                graph.remove(pattern_match.vertex(vertex).unwrap());
            });
        });

        self.edge_create.iter().for_each(|edge_create| {
            let source = get_rule_vertex(edge_create.source);
            let target = get_rule_vertex(edge_create.target);
            let value = (edge_create.creator)();
            graph.connect(source, target, value);
        });

        self.edge_delete.iter().for_each(|&edge| {
            graph.remove_edge(pattern_match.edge(edge).unwrap());
        });

        self.vertex_delete.iter().for_each(|&vertex| {
            let vertex = pattern_match.vertex(vertex).unwrap();
            #[cfg(test)]
            println!("deleting {vertex:?}");
            graph.remove(vertex);
        });

        true
    }
}
