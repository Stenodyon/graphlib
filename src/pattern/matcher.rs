use std::collections::HashMap;

use crate::Graph;

use super::selector::{EdgeKind, VertexKind};

pub struct MatchContext<'a, P, G, Filter>
where
    P: Graph<VertexLabel = VertexKind, EdgeLabel = EdgeKind>,
    G: Graph,
{
    pattern: &'a P,
    filter: Filter,
    graph: &'a G,
    decided_vertices: Vec<(P::VertexIndex, G::VertexIndex)>,
    decided_edges: Vec<(P::EdgeIndex, G::EdgeIndex)>,
}

impl<'a, P, G, F> MatchContext<'a, P, G, F>
where
    P: Graph<VertexLabel = VertexKind, EdgeLabel = EdgeKind>,
    G: Graph,
    F: Filter<<G as Graph>::VertexLabel, <G as Graph>::EdgeLabel>,
{
    pub fn new(pattern: &'a P, graph: &'a G, filter: F) -> Self {
        let decided_vertices = Vec::with_capacity(graph.vertex_count());
        let decided_edges = Vec::with_capacity(graph.edge_count());

        Self {
            pattern,
            filter,
            graph,
            decided_vertices,
            decided_edges,
        }
    }

    fn decision_state(&self) -> (usize, usize) {
        (self.decided_vertices.len(), self.decided_edges.len())
    }

    fn restore_decisions(&mut self, (vertices, edges): (usize, usize)) {
        self.decided_vertices
            .resize_with(vertices, || unreachable!());
        self.decided_edges.resize_with(edges, || unreachable!());
    }

    fn with_vertex_decision(
        &mut self,
        vertex: P::VertexIndex,
        choice: G::VertexIndex,
        mut with_decision: impl FnMut(&mut Self, G::VertexIndex) -> bool,
    ) -> bool {
        if let Some(decided) = self.get_vertex_decision(vertex) {
            return decided == choice;
        }

        #[cfg(test)]
        println!("- deciding on {vertex:?} -> {choice:?}");

        // === Check Constraints ===
        // 1. Check vertex selector graph constraint.
        if !(self
            .pattern
            .vertex_label(vertex)
            .unwrap()
            .is_valid(self.graph, choice)
            && self.filter.filter_vertex(self.graph, choice))
        {
            #[cfg(test)]
            println!("\tfiltered out by selector");

            return false;
        }

        // 2. Check distinct vertices.
        if self.pattern.vertex_label(vertex).unwrap() == VertexKind::Distinct
            && self
                .decided_vertices
                .iter()
                .any(|(_, vertex)| *vertex == choice)
        {
            #[cfg(test)]
            println!("\tnot distinct");

            return false;
        }

        // 3. Check outgoing edges constraints.
        for (edge, target) in self.pattern.outgoing_edges(vertex).filter_map(|edge| {
            let edge_to_self =
                self.pattern.source(edge).unwrap() == self.pattern.target(edge).unwrap();
            self.get_vertex_decision(self.pattern.target(edge).unwrap())
                .or(edge_to_self.then_some(choice))
                .map(|target| (edge, target))
        }) {
            if let Some(edge_choice) = self.get_edge_decision(edge) {
                if self.graph.source(edge_choice).unwrap() != choice {
                    #[cfg(test)]
                    println!("\toutgoing edge didn't match already selected");

                    return false;
                }
            }

            if !self
                .pattern
                .edge_label(edge)
                .unwrap()
                .is_valid(self.graph, choice, target)
            {
                #[cfg(test)]
                println!("\toutgoing edge filtered out by selector");

                return false;
            }
        }

        // 4. Check incoming edges constraints.
        for (edge, source) in self.pattern.incoming_edges(vertex).filter_map(|edge| {
            let edge_to_self =
                self.pattern.source(edge).unwrap() == self.pattern.target(edge).unwrap();
            self.get_vertex_decision(self.pattern.source(edge).unwrap())
                .or(edge_to_self.then_some(choice))
                .map(|source| (edge, source))
        }) {
            if let Some(edge_choice) = self.get_edge_decision(edge) {
                if self.graph.target(edge_choice).unwrap() != choice {
                    #[cfg(test)]
                    println!("\tincoming edge didn't match already selected");

                    return false;
                }
            }

            if !self
                .pattern
                .edge_label(edge)
                .unwrap()
                .is_valid(self.graph, source, choice)
            {
                #[cfg(test)]
                println!("\tincoming edge filtered out by selector");

                return false;
            }
        }

        let snapshot = self.decision_state();
        self.decided_vertices.push((vertex, choice));
        if with_decision(self, choice) {
            #[cfg(test)]
            println!("- decided on {vertex:?} -> {choice:?}");

            return true;
        }
        self.restore_decisions(snapshot);
        false
    }

    fn with_edge_decision(
        &mut self,
        edge: P::EdgeIndex,
        choice: G::EdgeIndex,
        mut with_decision: impl FnMut(&mut Self, G::EdgeIndex) -> bool,
    ) -> bool {
        if let Some(decided) = self.get_edge_decision(edge) {
            return decided == choice;
        }

        #[cfg(test)]
        println!("- deciding on {edge:?} -> {choice:?}");

        match self.pattern.edge_label(edge).unwrap() {
            EdgeKind::Absent | EdgeKind::Accessible => return false,
            _ => {}
        }

        if !self.filter.filter_edge(self.graph, choice) {
            return false;
        }

        if let Some(source) = self.get_vertex_decision(self.pattern.source(edge).unwrap()) {
            if source != self.graph.source(choice).unwrap() {
                #[cfg(test)]
                println!("\t mismatch on source vertex decision");

                return false;
            }
        }

        if let Some(target) = self.get_vertex_decision(self.pattern.target(edge).unwrap()) {
            if target != self.graph.target(choice).unwrap() {
                #[cfg(test)]
                println!("\t mismatch on target vertex decision");

                return false;
            }
        }

        let snapshot = self.decision_state();
        self.decided_edges.push((edge, choice));
        if with_decision(self, choice) {
            #[cfg(test)]
            println!("- decided on {edge:?} -> {choice:?}");

            return true;
        }
        self.restore_decisions(snapshot);
        false
    }

    fn get_vertex_decision(&self, vertex: P::VertexIndex) -> Option<G::VertexIndex> {
        self.decided_vertices
            .iter()
            .find(|(from, _)| *from == vertex)
            .map(|(_, to)| *to)
    }

    fn get_edge_decision(&self, edge: P::EdgeIndex) -> Option<G::EdgeIndex> {
        self.decided_edges
            .iter()
            .find(|(from, _)| *from == edge)
            .map(|(_, to)| *to)
    }

    fn decide_vertex(
        &mut self,
        vertex: P::VertexIndex,
        mut choices: impl Iterator<Item = G::VertexIndex>,
        mut with_decision: impl FnMut(&mut Self, G::VertexIndex) -> bool,
    ) -> bool {
        if let Some(choice) = self.get_vertex_decision(vertex) {
            return with_decision(self, choice);
        }

        choices.any(|choice| self.with_vertex_decision(vertex, choice, &mut with_decision))
    }

    fn edge_source_candidates(&self, edge: P::EdgeIndex) -> Vec<G::VertexIndex> {
        let target = self.pattern.target(edge).unwrap();
        let target_decision = self.get_vertex_decision(target);
        let edge_selector = self.pattern.edge_label(edge).unwrap();
        if edge_selector == EdgeKind::Accessible || target_decision.is_none() {
            self.graph.vertex_indices().collect()
        } else {
            let Some(target) = target_decision else {
                unreachable!()
            };

            self.graph
                .vertex_indices()
                .filter(move |&vertex| edge_selector.is_valid(&self.graph, vertex, target))
                .collect()
        }
    }

    fn edge_target_candidates(&self, edge: P::EdgeIndex) -> Vec<G::VertexIndex> {
        let source = self.pattern.source(edge).unwrap();
        let source_decision = self.get_vertex_decision(source);
        let edge_selector = self.pattern.edge_label(edge).unwrap();
        if edge_selector == EdgeKind::Accessible || source_decision.is_none() {
            self.graph.vertex_indices().collect()
        } else {
            let Some(source) = source_decision else {
                unreachable!()
            };

            self.graph
                .vertex_indices()
                .filter(move |&vertex| edge_selector.is_valid(&self.graph, source, vertex))
                .collect()
        }
    }

    fn decide_propagate_edge(&mut self, edge: P::EdgeIndex) -> bool {
        let source = self.pattern.source(edge).unwrap();
        let target = self.pattern.target(edge).unwrap();

        let edge_selector = self.pattern.edge_label(edge).unwrap();

        if self.get_vertex_decision(source).is_none()
            && !self
                .edge_source_candidates(edge)
                .into_iter()
                .any(|source_choice| {
                    self.with_vertex_decision(source, source_choice, |this, _| {
                        this.decide_propagate_vertex(source)
                    })
                })
        {
            return false;
        }

        if self.get_vertex_decision(target).is_none()
            && !self
                .edge_target_candidates(edge)
                .into_iter()
                .any(|target_choice| {
                    self.with_vertex_decision(target, target_choice, |this, _| {
                        this.decide_propagate_vertex(target)
                    })
                })
        {
            return false;
        }

        let source_choice = self.get_vertex_decision(source).unwrap();
        let target_choice = self.get_vertex_decision(target).unwrap();

        match edge_selector {
            EdgeKind::Present | EdgeKind::Schism => {
                if self.get_edge_decision(edge).is_some() {
                    return true;
                }

                let Some(edge_choice) = self.graph.find_edge(source_choice, target_choice) else {
                    return false;
                };

                self.with_edge_decision(edge, edge_choice, |_, _| true)
            }

            EdgeKind::Absent | EdgeKind::Accessible => true,
        }
    }

    fn decide_propagate_vertex(&mut self, vertex: P::VertexIndex) -> bool {
        let with_decision = |this: &mut Self, _| {
            let incoming = this
                .pattern
                .incoming_edges(vertex)
                .all(|edge| this.decide_propagate_edge(edge));

            if !incoming {
                #[cfg(test)]
                println!("\tcould not satisfy incoming edges for {vertex:?}");

                return false;
            }

            let outgoing = this
                .pattern
                .outgoing_edges(vertex)
                .all(|edge| this.decide_propagate_edge(edge));

            #[cfg(test)]
            if !outgoing {
                println!("\tcould not satisfy outgoing edges for {vertex:?}");
            }

            outgoing
        };

        if let Some(choice) = self.get_vertex_decision(vertex) {
            with_decision(self, choice)
        } else {
            self.decide_vertex(vertex, self.graph.vertex_indices(), with_decision)
        }
    }

    pub fn find_match(mut self) -> Option<Match<P, G>> {
        if self
            .pattern
            .vertex_indices()
            .all(|vertex| self.decide_propagate_vertex(vertex))
        {
            Some(MatchResult {
                vertices: self.decided_vertices.into_iter().collect(),
                edges: self.decided_edges.into_iter().collect(),
            })
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct MatchResult<PV, GV, PE, GE>
where
    PV: std::hash::Hash,
    PE: std::hash::Hash,
{
    pub vertices: HashMap<PV, GV>,
    pub edges: HashMap<PE, GE>,
}

pub type Match<P, G> = MatchResult<
    <P as Graph>::VertexIndex,
    <G as Graph>::VertexIndex,
    <P as Graph>::EdgeIndex,
    <G as Graph>::EdgeIndex,
>;

impl<PV, GV, PE, GE> MatchResult<PV, GV, PE, GE>
where
    PV: PartialEq + Eq + std::hash::Hash,
    PE: PartialEq + Eq + std::hash::Hash,
    GV: Copy,
    GE: Copy,
{
    #[inline(always)]
    pub fn vertex(&self, vertex: PV) -> Option<GV> {
        self.vertices.get(&vertex).copied()
    }

    #[inline(always)]
    pub fn edge(&self, edge: PE) -> Option<GE> {
        self.edges.get(&edge).copied()
    }
}

pub trait Filter<V, E> {
    fn filter_vertex<G>(&self, graph: &G, vertex: <G as Graph>::VertexIndex) -> bool
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>;

    fn filter_edge<G>(&self, graph: &G, edge: <G as Graph>::EdgeIndex) -> bool
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct NoFilter;

impl<V, E> Filter<V, E> for NoFilter {
    fn filter_vertex<G>(&self, _graph: &G, _vertex: <G as Graph>::VertexIndex) -> bool
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
    {
        true
    }

    fn filter_edge<G>(&self, _graph: &G, _edge: <G as Graph>::EdgeIndex) -> bool
    where
        G: Graph<VertexLabel = V, EdgeLabel = E>,
    {
        true
    }
}
