
use crate::{Directed, VecGraph};

use super::*;

type PatternGraph = VecGraph<Directed, VertexKind, EdgeKind>;

#[test]
fn single_vertex() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    let a = graph.insert("a");
    let mut pattern = PatternGraph::new();
    let n = pattern.insert(VertexKind::Present);

    let pattern = &pattern;

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(n), Some(a));

    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn three_chain() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    graph.connect(a, b, ());
    graph.connect(b, c, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pc = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pb, EdgeKind::Present);
    pattern.connect(pb, pc, EdgeKind::Present);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pa), Some(a));
    assert_eq!(match_result.vertex(pb), Some(b));
    assert_eq!(match_result.vertex(pc), Some(c));

    for _ in 0..10 {
        assert!(pattern
            .copied()
            .find_random_match(&graph, &mut rand::thread_rng())
            .is_some());
    }
}

#[test]
fn three_chain_regression01() {
    let mut graph = VecGraph::new_directed();
    let c = graph.insert("c");
    let b = graph.insert("b");
    let a = graph.insert("a");
    graph.connect(a, b, ());
    graph.connect(b, c, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pc = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pb, EdgeKind::Present);
    pattern.connect(pb, pc, EdgeKind::Present);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pa), Some(a));
    assert_eq!(match_result.vertex(pb), Some(b));
    assert_eq!(match_result.vertex(pc), Some(c));
}

#[test]
fn three_vertices() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert("a");
    graph.insert("b");
    graph.insert("c");
    let mut pattern = PatternGraph::new();
    pattern.insert(VertexKind::Present);
    pattern.insert(VertexKind::Present);
    pattern.insert(VertexKind::Present);
    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn single_edge() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let e = graph.connect(a, b, ());
    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pe = pattern.connect(pa, pb, EdgeKind::Present);
    match pattern.copied().find_match(&graph) {
        Some(match_result) => {
            assert_eq!(match_result.vertex(pa), Some(a));
            assert_eq!(match_result.vertex(pb), Some(b));
            assert_eq!(match_result.edge(pe), Some(e));
        }
        None => unreachable!(),
    }

    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn no_single_edge() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert("a");
    graph.insert("b");
    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pb, EdgeKind::Present);
    assert!(pattern.copied().find_match(&graph).is_none());
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_none());
}

#[test]
fn to_self() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let e = graph.connect(a, a, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pe = pattern.connect(pa, pb, EdgeKind::Present);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pa), Some(a));
    assert_eq!(match_result.vertex(pb), Some(a));
    assert_eq!(match_result.edge(pe), Some(e));

    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn absent_edge_self() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    graph.connect(a, a, ());

    let mut pattern = PatternGraph::new();
    let pb = pattern.insert(VertexKind::Present);
    pattern.connect(pb, pb, EdgeKind::Absent);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none());
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_none());
}

#[test]
fn absent_edge_self2() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    graph.insert("b");
    graph.connect(a, a, ());

    let mut pattern = PatternGraph::new();
    let pb = pattern.insert(VertexKind::Present);
    pattern.connect(pb, pb, EdgeKind::Absent);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_ne!(match_result.vertex(pb), Some(a));
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn absent_edge() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());
    graph.connect(b, a, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pb, EdgeKind::Absent);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pa), match_result.vertex(pb));

    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn no_schism_self() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert("a");

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pb, EdgeKind::Schism);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none(), "match_result = {match_result:#?}");
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_none());
}

#[test]
fn schism() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let e = graph.connect(a, b, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pe = pattern.connect(pa, pb, EdgeKind::Schism);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pa), Some(a));
    assert_eq!(match_result.vertex(pb), Some(b));
    assert_eq!(match_result.edge(pe), Some(e));

    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());

    let c = graph.insert("c");
    graph.connect(a, c, ());
    graph.connect(c, a, ());
    graph.connect(c, b, ());
    assert!((&graph).without_edge(e).is_reachable(a, b));

    let pc = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pc, EdgeKind::Present);
    pattern.connect(pc, pb, EdgeKind::Present);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none(), "match_result = {match_result:#?}");
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_none());
}

#[test]
fn schism_back_and_forth() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    let c = graph.insert("c");
    let d = graph.insert("d");
    graph.connect(a, b, ());
    graph.connect(b, a, ());
    graph.connect(c, d, ());
    graph.connect(d, c, ());
    graph.connect(a, c, ());
    graph.connect(d, b, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pc = pattern.insert(VertexKind::Present);
    let pd = pattern.insert(VertexKind::Present);
    pattern.connect(pa, pc, EdgeKind::Present);
    pattern.connect(pc, pd, EdgeKind::Schism);
    pattern.connect(pd, pb, EdgeKind::Present);
    pattern.connect(pb, pa, EdgeKind::Schism);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    assert!(pattern
        .copied()
        .find_random_match(&graph, &mut rand::thread_rng())
        .is_some());
}

#[test]
fn leaf() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());

    let mut pattern = PatternGraph::new();
    let pb = pattern.insert(VertexKind::Leaf);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    assert_eq!(match_result.vertex(pb), Some(b));
}

#[test]
fn no_leaf() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());
    graph.connect(b, a, ());

    let mut pattern = PatternGraph::new();
    pattern.insert(VertexKind::Leaf);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none());
}

#[test]
fn self_not_leaf() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");
    graph.connect(a, b, ());
    graph.connect(b, b, ());

    let mut pattern = PatternGraph::new();
    pattern.insert(VertexKind::Leaf);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none());
}

#[test]
fn no_distinct() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert("a");

    let mut pattern = PatternGraph::new();
    pattern.insert(VertexKind::Distinct);
    pattern.insert(VertexKind::Distinct);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_none(), "match_result = {match_result:#?}");
}

#[test]
fn distinct() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    let a = graph.insert("a");
    let b = graph.insert("b");

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Distinct);
    let pb = pattern.insert(VertexKind::Distinct);

    let match_result = pattern.copied().find_match(&graph);
    assert!(match_result.is_some());
    let match_result = match_result.unwrap();
    let pb_val = if match_result.vertex(pa) == Some(a) {
        Some(b)
    } else if match_result.vertex(pb) == Some(b) {
        Some(a)
    } else {
        unreachable!()
    };
    assert_eq!(match_result.vertex(pb), pb_val);
}
