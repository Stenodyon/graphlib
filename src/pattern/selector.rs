use crate::Graph;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum VertexKind {
    Distinct,
    Present,
    Leaf,
}

impl VertexKind {
    pub fn is_valid<G: Graph>(self, graph: &G, vertex: G::VertexIndex) -> bool {
        match self {
            VertexKind::Distinct => true,
            VertexKind::Present => true,
            VertexKind::Leaf => graph.outgoing_edges(vertex).count() == 0,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum EdgeKind {
    Present,
    Absent,
    Accessible,
    Schism,
}

impl EdgeKind {
    pub fn is_valid<G: Graph>(
        self,
        graph: &G,
        source: G::VertexIndex,
        target: G::VertexIndex,
    ) -> bool {
        match self {
            EdgeKind::Present => graph.are_connected(source, target),
            EdgeKind::Absent => !graph.are_connected(source, target),
            EdgeKind::Accessible => graph.is_reachable(source, target),

            EdgeKind::Schism => graph
                .find_edge(source, target)
                .map(|edge| !graph.without_edge(edge).is_reachable(source, target))
                .unwrap_or(false),
        }
    }
}
