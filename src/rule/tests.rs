use crate::pattern::selector::{EdgeKind, VertexKind};

use super::*;

type PatternGraph = VecGraph<Directed, VertexKind, EdgeKind>;

#[test]
fn remove_single_vertex() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert(());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);

    let pattern = (&pattern).copied();

    let rule = Rule {
        vertex_delete: vec![pa],
        ..Rule::new(pattern)
    };

    assert_eq!(graph.vertex_count(), 1);
    assert!(rule.try_apply(&mut graph, NoProcessing));
    assert_eq!(graph.vertex_count(), 0);
}

#[test]
fn remove_single_edge() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert(());
    let b = graph.insert(());
    graph.connect(a, b, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Present);
    let pb = pattern.insert(VertexKind::Present);
    let pe = pattern.connect(pa, pb, EdgeKind::Present);

    let pattern = (&pattern).copied();

    let rule = Rule {
        edge_delete: vec![pe],
        ..Rule::new(pattern)
    };

    assert_eq!(graph.edge_count(), 1);
    assert!(rule.try_apply(&mut graph, NoProcessing));
    assert_eq!(graph.vertex_count(), 2);
    assert_eq!(graph.edge_count(), 0);
    assert!(!graph.are_connected(a, b));
}

#[test]
fn merge_vertices() {
    let mut graph = VecGraph::<_, _, ()>::new_directed();
    graph.insert(1usize);
    graph.insert(2);
    graph.insert(3);

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Distinct);
    let pb = pattern.insert(VertexKind::Distinct);
    let pc = pattern.insert(VertexKind::Distinct);

    let pattern = (&pattern).copied();

    let rule = Rule {
        vertex_merge: vec![Merge {
            witness: pa,
            merges: vec![pb, pc],
        }],
        ..Rule::new(pattern)
    };

    assert_eq!(graph.vertex_count(), 3);
    assert!(rule.try_apply(&mut graph, Adder));
    assert_eq!(graph.vertex_count(), 1);
    assert_eq!(
        *graph.get(graph.vertex_indices().next().unwrap()).unwrap(),
        6
    );

    struct Adder;

    impl RuleProcessor<usize, ()> for Adder {
        fn process_vertices(&mut self, processed: &mut usize, merging: &[&usize]) {
            merging
                .iter()
                .copied()
                .copied()
                .for_each(|value| *processed += value);
        }
    }
}

#[test]
fn merge_edges() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert(());
    let b = graph.insert(());
    let c = graph.insert(());
    let d = graph.insert(());
    graph.connect(a, d, 1usize);
    graph.connect(b, d, 2);
    graph.connect(c, d, 3);

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Distinct);
    let pb = pattern.insert(VertexKind::Distinct);
    let pc = pattern.insert(VertexKind::Distinct);
    let pd = pattern.insert(VertexKind::Distinct);
    let e1 = pattern.connect(pa, pd, EdgeKind::Present);
    let e2 = pattern.connect(pb, pd, EdgeKind::Present);
    let e3 = pattern.connect(pc, pd, EdgeKind::Present);

    let pattern = (&pattern).copied();

    let rule = Rule {
        edge_merge: vec![Merge {
            witness: e1,
            merges: vec![e2, e3],
        }],
        ..Rule::new(pattern)
    };

    assert_eq!(graph.edge_count(), 3);
    assert!(rule.try_apply(&mut graph, Adder));
    assert_eq!(graph.edge_count(), 1);
    assert_eq!(
        *graph
            .get_edge(graph.edge_indices().next().unwrap())
            .unwrap(),
        6
    );

    struct Adder;

    impl RuleProcessor<(), usize> for Adder {
        fn process_edges(&mut self, processed: &mut usize, merging: &[&usize]) {
            merging
                .iter()
                .copied()
                .copied()
                .for_each(|value| *processed += value);
        }
    }
}

#[test]
fn move_edge_delete_vertex() {
    let mut graph = VecGraph::new_directed();
    let a = graph.insert(());
    let b = graph.insert(());
    let c = graph.insert(());
    graph.connect(a, b, ());

    let mut pattern = PatternGraph::new();
    let pa = pattern.insert(VertexKind::Distinct);
    let pb = pattern.insert(VertexKind::Distinct);
    let pc = pattern.insert(VertexKind::Distinct);
    let pe = pattern.connect(pa, pb, EdgeKind::Present);

    let pattern = (&pattern).copied();

    let rule = Rule {
        edge_change_target: vec![(pe, RuleVertex::KeptNode(pc))],
        vertex_delete: vec![pb],
        ..Rule::new(pattern)
    };

    assert!(rule.try_apply(&mut graph, NoProcessing));
    assert_eq!(graph.vertex_count(), 2);
    assert_eq!(graph.edge_count(), 1);
    assert!(graph.are_connected(a, c));
}
